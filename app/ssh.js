const fs = require('fs')
const path = require('path')
const node_ssh = require('node-ssh')
const mkdirp = require('mkdirp');
const ssh = new node_ssh()
const config = require('./util/config.json');



let filesData = [];

// Read text from the input file
(async function readDataFromInputFile() {
  await fs.readFile(config[1].inputFileName, function (err, data) {
    if (err) {
      throw err;
    } else {
      let filterFilesData = data.toString().split('\n');
      for (let i = 0; i < filterFilesData.length; i++) {
        filesData.push(filterFilesData[i]);
      }
      console.log(filesData);
    }
  })
}());


//Create local directory i.e D:/test/abc
function createDir(dirPath) {
    mkdirp(config[0].dirPath + dirPath, function (err) {
      if (err) console.error(err)
      else console.log('Directory created!!!');
    });
  }

ssh.connect({
    host: '10.34.53.152',
    username: 'ec2-user',
    privateKey: 'OLPRecovery.ppk'
  })
  .then(function() {
    console.log( 'started!!!!');
    for (let i = 0; i < filesData.length; i++) {
        createDir(path.dirname(filesData[i]));
        ssh.getFile(config[0].dirPath+path.dirname(filesData[i])+'/'+path.basename(filesData[i]).replace(/[\r\n]/g, ""), filesData[i].replace(/[\r\n]/g, "")).then(function(Contents) {
            console.log("The File's contents were successfully downloaded")
          }, function(error) {
            console.log("Something's wrong")
            console.log(error)
          })
    }
    
  });