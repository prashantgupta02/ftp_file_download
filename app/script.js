//----------------------------
//Api modules
//----------------------------
let fs = require('fs');
let Client = require('ftp');
let path = require('path');
let mkdirp = require('mkdirp');
let clientRef = new Client();

//-------------------------------------------------------
//local property file
//-------------------------------------------------------
let config = require('./util/config.json');

let filesData = [];

//-------------------------------------------------------
// Read text from the input file
//-------------------------------------------------------
(async function readDataFromInputFile() {
  await fs.readFile(process.argv[2] + "/" + process.argv[3], function (err, data) {
    if (err) {
      throw err;
    } else {
      let filterFilesData = data.toString().split('\n');
      console.log(`Reading input text file...`);
      for (let i = 0; i < filterFilesData.length; i++) {
        filesData.push(filterFilesData[i]);
      }
    }
  })
}());

//-------------------------------------------------------------
// Download file from FTP server and store in local environment
//-------------------------------------------------------------
clientRef.on('ready', function () {
	console.log(`Sever connected...`);
  console.log(`file checkout started...`);
  for (let i = 0; i < filesData.length; i++) {
    mkdirp(process.argv[2] + "/" + path.dirname(filesData[i]), function (err) {
      if (err) console.error(err)
    });
    clientRef.get(filesData[i], function (err, stream) {
      if (err) throw err;
      stream.once('close', function () {
        clientRef.end();
      });
      stream.pipe(fs.createWriteStream(process.argv[2] + "/" + path.dirname(filesData[i]) + "/" + path.basename(filesData[i]).replace(/[\r\n]/g, "")));
      if (filesData.length - 1 == i) {
        console.log(`Files are successfully downloaded at ${process.argv[2]}`);
      }
    });
  }
});

//-------------------------------------------------------
// Create connection with FTP server
//-------------------------------------------------------
clientRef.connect({
  host: config[0].host,
  user: config[0].user,
  password: config[0].password
  
});
